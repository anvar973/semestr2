package ru.omsu.imit.catalog;

import org.junit.Test;
import ru.omsu.imit.catalog.model.Developer;
import ru.omsu.imit.catalog.rest.response.DeveloperResponse;
import ru.omsu.imit.catalog.utils.ErrorCode;

public class DeveloperMethodsTest extends BaseClientTest {
    @Test
    public void addAuthorTest() {
        Developer developer = new Developer("Richard");
        addDeveloper(developer, ErrorCode.SUCCESS);
    }

    @Test
    public void getAuthorByIdTest() {
        Developer developer = new Developer("Maria");
        DeveloperResponse developerResponse = addDeveloper(developer, ErrorCode.SUCCESS);
        getDeveloper(developerResponse.getId(), developer, ErrorCode.SUCCESS);
    }

    @Test
    public void getAllAuthorsTest() {
        Developer developer1 = new Developer("Richard");
        Developer developer2 = new Developer("Maria");
        Developer developer3 = new Developer("Daniel");
        addDeveloper(developer1, ErrorCode.SUCCESS);
        addDeveloper(developer2, ErrorCode.SUCCESS);
        addDeveloper(developer3, ErrorCode.SUCCESS);
        getAllDevelopers(3, ErrorCode.SUCCESS);
    }

    @Test
    public void deleteAuthorByIdTest() {
        Developer developer = new Developer("Daniel");
        DeveloperResponse response = addDeveloper(developer, ErrorCode.SUCCESS);
        getDeveloper(response.getId(), developer, ErrorCode.SUCCESS);
        deleteDeveloperById(response.getId(), ErrorCode.SUCCESS);
        getDeveloper(response.getId(), developer, ErrorCode.ITEM_NOT_FOUND);
    }


    @Test
    public void addWithWrongNameTest() {
        Developer developer1 = new Developer(null);
        Developer developer2 = new Developer("Tony");
        Developer developer3 = new Developer("Garry");
        addDeveloper(developer1, ErrorCode.NULL_FIELD);
        addDeveloper(developer2, ErrorCode.SUCCESS);
        addDeveloper(developer3, ErrorCode.SUCCESS);
    }

    @Test
    public void getByWrongIdTest() {
        Developer developer = new Developer("Maria");
        addDeveloper(developer, ErrorCode.SUCCESS);
        getDeveloper(10000, developer, ErrorCode.ITEM_NOT_FOUND);
    }

    @Test
    public void deleteByWrongIdTest() {
        Developer developer = new Developer("Daniel");
        addDeveloper(developer, ErrorCode.SUCCESS);
        deleteDeveloperById(100000, ErrorCode.ITEM_NOT_FOUND);
    }

}
