package ru.omsu.imit.catalog;

import org.junit.Assume;
import org.junit.BeforeClass;
import ru.omsu.imit.catalog.dao.CategoryDAO;
import ru.omsu.imit.catalog.dao.DeveloperDAO;
import ru.omsu.imit.catalog.dao.ProgramDAO;
import ru.omsu.imit.catalog.daoimpl.CategoryDAOImpl;
import ru.omsu.imit.catalog.daoimpl.DeveloperDAOImpl;
import ru.omsu.imit.catalog.daoimpl.ProgramDAOImpl;
import ru.omsu.imit.catalog.model.Program;
import ru.omsu.imit.catalog.rest.request.ProgramRequest;
import ru.omsu.imit.catalog.rest.request.DeveloperRequest;
import ru.omsu.imit.catalog.rest.request.CategoryRequest;
import ru.omsu.imit.catalog.rest.response.ProgramResponse;
import ru.omsu.imit.catalog.rest.response.DeveloperResponse;
import ru.omsu.imit.catalog.rest.response.CategoryResponse;
import ru.omsu.imit.catalog.utils.MyBatisUtils;

import java.util.Set;

import static org.junit.Assert.assertEquals;

public class CheckTest {

    protected DeveloperDAO authorDAO = new DeveloperDAOImpl();
    protected CategoryDAO categoryDAO = new CategoryDAOImpl();
    protected ProgramDAO sectionDAO = new ProgramDAOImpl();




    @BeforeClass()
    public static void init() {
        Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
    }

    protected static void checkDeveloperFields(DeveloperRequest dev1, DeveloperResponse dev2) {
        assertEquals(dev1.getName(), dev2.getName());
    }

    protected static void checkCategoryFields(CategoryRequest cat1, CategoryResponse cat2) {
        assertEquals(cat1.getName(), cat2.getName());
        assertEquals(cat1.getPrograms(), cat2.getPrograms());
    }

    protected static void checkProgramFields(ProgramRequest pr1, ProgramResponse pr2) {
        assertEquals(pr1.getName(), pr2.getName());
        assertEquals(pr1.getCreationDate(), pr2.getCreation_date());
        assertEquals(pr1.getLast_update_date(), pr2.getLast_update_date());
        assertEquals(pr1.getDeveloperid(), pr1.getDeveloperid());
        assertEquals(pr1.getCategoryid(), pr2.getCategoryId());
    }

    protected static void checkPrograms(Set<Program> programs, Set<Program> programs1) {
        assertEquals(programs, programs1);
    }
}
