package ru.omsu.imit.catalog;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.catalog.client.CatalogClient;
import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.model.Category;
import ru.omsu.imit.catalog.model.Developer;
import ru.omsu.imit.catalog.model.Program;
import ru.omsu.imit.catalog.rest.request.ProgramRequest;
import ru.omsu.imit.catalog.rest.request.DeveloperRequest;
import ru.omsu.imit.catalog.rest.request.CategoryRequest;
import ru.omsu.imit.catalog.rest.response.*;
import ru.omsu.imit.catalog.server.CatalogServer;
import ru.omsu.imit.catalog.server.config.Settings;
import ru.omsu.imit.catalog.utils.ErrorCode;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Date;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BaseClientTest extends CheckTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseClientTest.class);

    protected static CatalogClient client = new CatalogClient();
    private static String baseURL;

    private static void initialize() {
        String hostName = null;
        try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            LOGGER.debug("Can't determine my own host name", e);
        }
        baseURL = "http://" + hostName + ":" + Settings.getRestHTTPPort() + "/api";
    }

    @BeforeClass
    public static void startServer() {
        initialize();
        CatalogServer.createServer();
    }

    @Before
    public void clearDeveloperTable() {
        authorDAO.deleteAll();
        sectionDAO.deleteAll();
    }

    @AfterClass
    public static void stopServer() {
        CatalogServer.stopServer();
    }

    private EmptySuccessResponse deleteById(Object response, ErrorCode expectedStatus) {
        if (response instanceof EmptySuccessResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }


    protected void checkFailureResponse(Object response, ErrorCode expectedStatus) {
        assertTrue(response instanceof FailureResponse);
        FailureResponse failureResponseObject = (FailureResponse) response;
        assertEquals(expectedStatus, failureResponseObject.getErrorCode());
    }


//----------------------------------------------------------------------------------------------------------------------

    protected DeveloperResponse addDeveloper(Developer developer, ErrorCode expectedStatus) {
        DeveloperRequest request = new DeveloperRequest(developer.getName());
        Object response = client.post(baseURL + "/catalog/developer", request, DeveloperResponse.class);
        if (response instanceof DeveloperResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            DeveloperResponse addDeveloperResponse = (DeveloperResponse) response;
            checkDeveloperFields(request, addDeveloperResponse);
            return addDeveloperResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected DeveloperResponse getDeveloper(int id, Developer developer, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/catalog/developer/" + id, DeveloperResponse.class);
        if (response instanceof DeveloperResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            DeveloperResponse developerResponse = (DeveloperResponse) response;
            DeveloperRequest expectedAuthor = new DeveloperRequest(developer.getName(), developer.getPrograms());
            checkDeveloperFields(expectedAuthor, developerResponse);
            return developerResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected List<DeveloperResponse> getAllDevelopers(int expeectedCount, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/catalog/author", List.class);
        if (response instanceof List<?>) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            List<DeveloperResponse> responseList = (List<DeveloperResponse>) response;
            assertEquals(expeectedCount, responseList.size());
            return responseList;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteDeveloperById(int id, ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/catalog/author/" + id, EmptySuccessResponse.class);
        return deleteById(response, expectedStatus);
    }

//-------------------------------------------------------------------------------------------------------------

    protected CategoryResponse addCategory(Category section, ErrorCode expectedStatus) {
        CategoryRequest request = new CategoryRequest(section.getName());
        Object response = client.post(baseURL + "/catalog/section", request, CategoryResponse.class);
        if (response instanceof CategoryResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            CategoryResponse categoryResponse = (CategoryResponse) response;
            checkCategoryFields(request, categoryResponse);
            return categoryResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected CategoryResponse getCategory(int id, String expectedName, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/catalog/section/" + id, CategoryResponse.class);
        if (response instanceof CategoryResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            CategoryResponse categoryResponse = (CategoryResponse) response;
            checkCategoryFields(new CategoryRequest(expectedName), categoryResponse);
            return categoryResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected Set<CategoryResponse> getAllSections(int expeectedCount, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/catalog/section", Set.class);
        if (response instanceof Set<?>) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            Set<CategoryResponse> responseList = (Set<CategoryResponse>) response;
            assertEquals(expeectedCount, responseList.size());
            return responseList;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteCategoryById(int id, ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/catalog/section/" + id, EmptySuccessResponse.class);
        return deleteById(response, expectedStatus);
    }

    //----------------------------------------------------------------------------------------------------------------------
    protected ProgramResponse addProgram(Program program, ErrorCode expectedStatus) {
        ProgramRequest request = new ProgramRequest(program.getName(), program.getCreation_date(), program.getLast_update_date(),
                program.getCategoryId(), program.getDeveloperId());
        Object response = client.post(baseURL + "/catalog/announcement", request, ProgramResponse.class);
        if (response instanceof ProgramResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            ProgramResponse addAnnouncement = (ProgramResponse) response;
            checkProgramFields(request, addAnnouncement);
            return addAnnouncement;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected ProgramResponse getProgramById(int id, String expectedName, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/catalog/program/" + id, ProgramResponse.class);
        if (response instanceof ProgramResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            ProgramResponse programResponse = (ProgramResponse) response;
            assertEquals(expectedName, programResponse.getName());
            return programResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected Set<ProgramResponse> getProgramByCategoryId(int sectionId, int expectedCount, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/catalog/announcement/section/" + sectionId, Set.class);
        return getProgramBunch(response, expectedCount, expectedStatus);
    }

    protected Set<ProgramResponse> getProgramByDeveloperId(int authorId, int expectedCount, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/catalog/announcement/author/" + authorId, Set.class);
        return getProgramBunch(response, expectedCount, expectedStatus);
    }


    protected Set<ProgramResponse> getAllPrograms(int expectedCount, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/catalog/announcement", Set.class);
        return getProgramBunch(response, expectedCount, expectedStatus);
    }

    protected Set<ProgramResponse> getProgramByDate(Date date, int expectedCount, ErrorCode expectedStatus) throws CatalogException {
        Object response = client.get(baseURL + "/catalog/announcement/date/" + date, Set.class);
        if (response == null)
            throw new CatalogException(ErrorCode.UNKNOWN_ERROR, " Wrong parameter");
        return getProgramBunch(response, expectedCount, expectedStatus);
    }

    protected EmptySuccessResponse deleteProgramById(int id, ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/catalog/announcement/" + id, EmptySuccessResponse.class);
        return deleteById(response, expectedStatus);
    }

    private Set<ProgramResponse> getProgramBunch(Object response, int expectedCount, ErrorCode expectedStatus) {
        if (response instanceof Set<?>) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            Set<ProgramResponse> responseSet = (Set<ProgramResponse>) response;
            assertEquals(expectedCount, responseSet.size());
            return responseSet;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

}
