package ru.omsu.imit.catalog;

import org.junit.Test;
import ru.omsu.imit.catalog.model.Category;
import ru.omsu.imit.catalog.rest.response.CategoryResponse;
import ru.omsu.imit.catalog.utils.ErrorCode;

public class CategoryMethodsTest extends BaseClientTest {

    @Test
    public void addTest() {
        Category category = new Category("KKK");
        addCategory(category, ErrorCode.SUCCESS);
    }

    @Test
    public void getByIdTest() {
        Category category = new Category("KKK");
        CategoryResponse addResponse = addCategory(category, ErrorCode.SUCCESS);
        getCategory(addResponse.getId(), category.getName(), ErrorCode.SUCCESS);
    }

    @Test
    public void getAllTest() {
        Category category1 = new Category("KKK");
        Category category2 = new Category("NNN");
        addCategory(category1, ErrorCode.SUCCESS);
        addCategory(category2, ErrorCode.SUCCESS);
        getAllSections(2, ErrorCode.SUCCESS);
    }

    @Test
    public void deleteByIdTest() {
        Category category = new Category("KKK");
        CategoryResponse response = addCategory(category, ErrorCode.SUCCESS);
        deleteCategoryById(response.getId(), ErrorCode.SUCCESS);
    }

    @Test
    public void addWithSameNamesTest() {
        Category category1 = new Category("KKK");
        Category category2 = new Category("NNN");
        addCategory(category1, ErrorCode.SUCCESS);
        addCategory(category2, ErrorCode.DUPLICATED_ITEM);
    }

    @Test
    public void addWithoutNameTest() {
        Category category = new Category("");
        addCategory(category, ErrorCode.NULL_FIELD);
    }

    @Test
    public void getByWrongIdTest() {
        Category category = new Category("KKK");
        addCategory(category, ErrorCode.SUCCESS);
        getCategory(1000, category.getName(), ErrorCode.ITEM_NOT_FOUND);
    }

    @Test
    public void deleteByWrongIdTest() {
        Category category = new Category("KKK");
        addCategory(category, ErrorCode.SUCCESS);
        deleteCategoryById(100000, ErrorCode.ITEM_NOT_FOUND);
    }
}
