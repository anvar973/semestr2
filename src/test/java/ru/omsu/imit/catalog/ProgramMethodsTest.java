package ru.omsu.imit.catalog;

import org.junit.Test;
import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.model.Category;
import ru.omsu.imit.catalog.model.Developer;
import ru.omsu.imit.catalog.model.Program;
import ru.omsu.imit.catalog.rest.response.ProgramResponse;
import ru.omsu.imit.catalog.rest.response.DeveloperResponse;
import ru.omsu.imit.catalog.rest.response.CategoryResponse;
import ru.omsu.imit.catalog.utils.ErrorCode;

import java.sql.Date;

public class ProgramMethodsTest extends BaseClientTest {

    @Test
    public void addTest() {
        Developer developer = new Developer("Daniel");
        Category category = new Category("KKK");

        DeveloperResponse responseAuthor = addDeveloper(developer, ErrorCode.SUCCESS);
        CategoryResponse responseSection = addCategory(category, ErrorCode.SUCCESS);

        Program program = new Program("KLM", Date.valueOf("2015-03-6"), Date.valueOf("2018-02-12"),
                responseSection.getId(), responseAuthor.getId());
        addProgram(program, ErrorCode.SUCCESS);
    }

    @Test
    public void getByIdTest() {
        Developer developer = new Developer("Daniel");
        Category category = new Category("MMM");

        DeveloperResponse responseAuthor = addDeveloper(developer, ErrorCode.SUCCESS);
        CategoryResponse responseSection = addCategory(category, ErrorCode.SUCCESS);

        Program program = new Program("MKL", Date.valueOf("2011-01-11"), Date.valueOf("2018-08-18"),
                responseSection.getId(), responseAuthor.getId());
        ProgramResponse responseAnnouncement = addProgram(program, ErrorCode.SUCCESS);

        getProgramById(responseAnnouncement.getId(), program.getName(), ErrorCode.SUCCESS);
    }

    @Test
    public void getBySectionIdTest() {
        Developer developer = new Developer("Daniel");
        Category section1 = new Category("MMM");
        Category section2 = new Category("LLL");

        DeveloperResponse developerResponse = addDeveloper(developer, ErrorCode.SUCCESS);
        CategoryResponse responseSection1 = addCategory(section1, ErrorCode.SUCCESS);
        CategoryResponse responseSection2 = addCategory(section2, ErrorCode.SUCCESS);

        Program program = new Program("MMM", Date.valueOf("2011-01-11"), Date.valueOf("2018-02-12"),
                responseSection1.getId(), developerResponse.getId());
        Program program1 = new Program("LLL", Date.valueOf("2012-02-12"), Date.valueOf("1000-02-12"),
                responseSection2.getId(), developerResponse.getId());

        addProgram(program, ErrorCode.SUCCESS);
        addProgram(program1, ErrorCode.SUCCESS);

        getProgramByCategoryId(responseSection1.getId(), 1, ErrorCode.SUCCESS);
    }

    @Test
    public void getByAuthorIdTest() {
        Developer developer = new Developer("Daniel");
        Developer developer1 = new Developer("Richard");
        Category category = new Category("category");

        DeveloperResponse developerResponse = addDeveloper(developer, ErrorCode.SUCCESS);
        DeveloperResponse developerResponse1 = addDeveloper(developer1, ErrorCode.SUCCESS);
        CategoryResponse categoryResponse = addCategory(category, ErrorCode.SUCCESS);

        Program program = new Program("AAA", Date.valueOf("2011-01-11"), Date.valueOf("2018-02-12"),
                categoryResponse.getId(), developerResponse.getId());
        Program program1 = new Program("Wrong text", Date.valueOf("1000-02-12"), Date.valueOf("2000-02-12"),
                categoryResponse.getId(), developerResponse1.getId());

        addProgram(program, ErrorCode.SUCCESS);
        addProgram(program1, ErrorCode.SUCCESS);

        getProgramByDeveloperId(developerResponse.getId(), 1, ErrorCode.SUCCESS);
    }

}
