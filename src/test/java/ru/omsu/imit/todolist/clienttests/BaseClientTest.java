package ru.omsu.imit.todolist.clienttests;

import static org.junit.Assert.*;

import ru.omsu.imit.todolist.client.TodoListClient;
import ru.omsu.imit.todolist.database.DataBaseEmulator;
import ru.omsu.imit.todolist.rest.request.TodoItemRequest;
import ru.omsu.imit.todolist.rest.response.TodoItemInfoResponse;
import ru.omsu.imit.todolist.rest.response.EmptySuccessResponse;
import ru.omsu.imit.todolist.rest.response.FailureResponse;
import ru.omsu.imit.todolist.server.TodoListServer;
import ru.omsu.imit.todolist.server.config.Settings;
import ru.omsu.imit.todolist.utils.ErrorCode;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

public class BaseClientTest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseClientTest.class);

	protected static TodoListClient client = new TodoListClient();
	private static String baseURL;

	private static void initialize() {
		String hostName = null;
		try {
			hostName = InetAddress.getLocalHost().getCanonicalHostName();
		} catch (UnknownHostException e) {
			LOGGER.debug("Can't determine my own host name", e);
		}
		baseURL = "http://" + hostName + ":" + Settings.getRestHTTPPort() + "/api";
	}

	@BeforeClass
	public static void startServer() {
		initialize();
		TodoListServer.createServer();
		}		
		

	@AfterClass
	public static void stopServer() {
			TodoListServer.stopServer();
	}

	@Before
	public void clearDataBase() {
		DataBaseEmulator.clear();
	}
	
	public static String getBaseURL() {
		return baseURL;
	}

	protected void checkFailureResponse(Object response, ErrorCode expectedStatus) {
		assertTrue(response instanceof FailureResponse);
		FailureResponse failureResponseObject = (FailureResponse) response;
		assertEquals(expectedStatus, failureResponseObject.getErrorCode());
	}
	
	protected TodoItemInfoResponse addTodoItem(String text, ErrorCode expectedStatus) {
		TodoItemRequest request = new TodoItemRequest(text);
		Object response = client.post(baseURL + "/todolist", request, TodoItemInfoResponse.class);
		if (response instanceof TodoItemInfoResponse) {
			assertEquals(ErrorCode.SUCCESS, expectedStatus);
			TodoItemInfoResponse addTodoItemResponse = (TodoItemInfoResponse) response;
			assertEquals(text, addTodoItemResponse.getText());
			return addTodoItemResponse;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected TodoItemInfoResponse getTodoItemById(int id, String expectedText, ErrorCode expectedStatus) {
		Object response = client.get(baseURL + "/todolist/" + id, TodoItemInfoResponse.class);
		if (response instanceof TodoItemInfoResponse) {
			assertEquals(ErrorCode.SUCCESS, expectedStatus);
			TodoItemInfoResponse getTodoItemResponse = (TodoItemInfoResponse) response;
			assertEquals(id, getTodoItemResponse.getId());
			assertEquals(expectedText, getTodoItemResponse.getText());
			return getTodoItemResponse;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected List<TodoItemInfoResponse> getAllTodoItems(int expeectedCount, ErrorCode expectedStatus) {
		Object response = client.get(baseURL + "/todolist/", List.class);
		if (response instanceof List<?>) {
			assertEquals(ErrorCode.SUCCESS, expectedStatus);
			@SuppressWarnings("unchecked")
			List<TodoItemInfoResponse> responseList = (List<TodoItemInfoResponse>) response;
			assertEquals(expeectedCount, responseList.size());
			return responseList;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	
	protected TodoItemInfoResponse editTodoItemById(int id, String text, ErrorCode expectedStatus) {
		TodoItemRequest request = new TodoItemRequest(text);
		Object response = client.put(baseURL + "/todolist/" + id , request, TodoItemInfoResponse.class);
		if (response instanceof TodoItemInfoResponse) {
			assertEquals(ErrorCode.SUCCESS, expectedStatus);
			TodoItemInfoResponse addTodoItemResponse = (TodoItemInfoResponse) response;
			assertEquals(text, addTodoItemResponse.getText());
			return addTodoItemResponse;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}
	
	protected EmptySuccessResponse deleteTodoItemById(int id, ErrorCode expectedStatus) {
		Object response = client.delete(baseURL + "/todolist/" + id , EmptySuccessResponse.class);
		if (response instanceof EmptySuccessResponse) {
			assertEquals(ErrorCode.SUCCESS, expectedStatus);
			return (EmptySuccessResponse)response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}
	
	
}
