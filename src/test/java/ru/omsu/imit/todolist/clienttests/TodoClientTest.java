package ru.omsu.imit.todolist.clienttests;

import ru.omsu.imit.todolist.rest.response.TodoItemInfoResponse;
import ru.omsu.imit.todolist.utils.ErrorCode;

import org.junit.Test;

public class TodoClientTest extends BaseClientTest {


	@Test
	public void testAddTodoItem() {
		addTodoItem("Сдать экзамен", ErrorCode.SUCCESS);

	}

	@Test
	public void testGetById() {
		TodoItemInfoResponse addResponse = addTodoItem("Сдать экзамен", ErrorCode.SUCCESS);
		getTodoItemById(addResponse.getId(), "Сдать экзамен", ErrorCode.SUCCESS);
	}

	@Test
	public void testGetByWrongId() {
		getTodoItemById(1000, "Сдать экзамен", ErrorCode.ITEM_NOT_FOUND);
	}

	@Test
	public void testGetAll() {
		addTodoItem("Сдать экзамен", ErrorCode.SUCCESS);
		addTodoItem("Сдать сессию", ErrorCode.SUCCESS);
		addTodoItem("Окончить институт", ErrorCode.SUCCESS);
		getAllTodoItems(3, ErrorCode.SUCCESS);
	}

	@Test
	public void testGetAllWhenEmpty() {
		getAllTodoItems(0, ErrorCode.SUCCESS);
	}

	@Test
	public void testEditById() {
		TodoItemInfoResponse addResponse = addTodoItem("Сдать экзамен", ErrorCode.SUCCESS);
		editTodoItemById(addResponse.getId(), "Пересдать экзамен", ErrorCode.SUCCESS);
	}
	
	@Test
	public void testEditByWrongId() {
		editTodoItemById(1000, "Пересдать экзамен", ErrorCode.ITEM_NOT_FOUND);
	}

	@Test
	public void testDeleteById() {
		TodoItemInfoResponse addResponse = addTodoItem("Сдать экзамен", ErrorCode.SUCCESS);
		deleteTodoItemById(addResponse.getId(), ErrorCode.SUCCESS);
	}
}