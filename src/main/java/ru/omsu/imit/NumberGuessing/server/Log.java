package ru.omsu.imit.NumberGuessing.server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;

public class Log {
    private static Path path = Paths.get("logs/numberGuessLogs.txt");

    private Log(){}

    static synchronized void log(int id,Date start, Date end, int attemps) throws IOException {
        try(BufferedWriter bw = Files.newBufferedWriter(path, StandardOpenOption.APPEND)){
            bw.write("Client id: "+ id);
            bw.newLine();
            bw.write("Start and finish date: "+ start+ " - "+ end);
            bw.newLine();
            bw.write("Attemps: "+attemps);
            bw.newLine();
            bw.write("-------------------------------------------------------------------------------------------------------------");
            bw.newLine();
        }
    }
}
