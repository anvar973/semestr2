package ru.omsu.imit.NumberGuessing.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Date;

public class ClientService extends Thread {

    private final int id;
    private final Socket socket;
    private final int randomNumber;
    private int attemps;
    private Date start;

    public ClientService(int id, Socket socket, int randomNumber) {
        this.id = id;
        this.socket = socket;
        this.randomNumber = randomNumber;
    }

    @Override
    public void run() {
        start = new Date();

        try (DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
             DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream())) {

            System.out.println("Client id = " + id + " connected and his num = " + randomNumber);

            dataOutputStream.writeUTF("Hello, u need to guess a number");
            boolean winflag = true;
            while (winflag) {
                attemps ++;
                int choice = dataInputStream.readInt();

                if (choice > randomNumber) {
                    dataOutputStream.writeUTF("<");
                    dataOutputStream.flush();
                }

                if (choice < randomNumber) {
                    dataOutputStream.writeUTF(">");
                    dataOutputStream.flush();
                }

                if (choice == randomNumber) {
                    dataOutputStream.writeUTF("Good game, well played, you won!!!");
                    dataOutputStream.flush();
                    winflag = false;
                }
            }

            Log.log(id,start,new Date(),attemps);
            System.out.println("Client id = " + id + "disconnected, his num was " + randomNumber);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
