package ru.omsu.imit.NumberGuessing.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private static final int port = 777;
    private static boolean winflag = true;

    public static void main(String[] args) {
        final ExecutorService executorService = Executors.newCachedThreadPool();

        try (ServerSocket serverSocket = new ServerSocket(port)) {
            System.out.println("Waiting clients");
            int id = 0;
            final Random random = new Random(0);
            while (winflag) {
                final Socket clientSocket = serverSocket.accept();
                final int randomNum = random.nextInt(1000);
                executorService.execute(new ClientService(id++, clientSocket, randomNum));
            }
            executorService.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
