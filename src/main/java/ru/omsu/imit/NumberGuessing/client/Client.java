package ru.omsu.imit.NumberGuessing.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        final int serverPort = 777;
        final String host = "localhost";
        int attempts = 0;

        try (Socket socket = new Socket(host, serverPort);
             DataInputStream dis = new DataInputStream(socket.getInputStream());
             DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
             Scanner scanner = new Scanner(System.in)) {

            final String hello = dis.readUTF();
            System.out.println(hello);
            System.out.println("Let's start guessing number");

            boolean goon = true;
            while (goon) {
                System.out.println("please, write num");
                final int num = scanner.nextInt();
                attempts++;

                dos.writeInt(num);
                dos.flush();

                final String response = dis.readUTF();
                System.out.println(response);

                goon = winflag(response);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean winflag(String string) {
        if (string.charAt(string.length() - 1) == '!')
            return false;

        return true;
    }
}
