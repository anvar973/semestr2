package ru.omsu.imit.catalog.exception;

import ru.omsu.imit.catalog.utils.ErrorCode;

public class CatalogException extends Exception {

	private static final long serialVersionUID = 6049904777923589329L;
	private ErrorCode errorCode;
	private String param;

	public CatalogException(ErrorCode errorCode, String param) {
		this.errorCode = errorCode;
		this.param = param;
	}

	public CatalogException(ErrorCode errorCode) {
		this(errorCode, null);
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public String getMessage() {
		if (param != null)
			return String.format(errorCode.getMessage(), param);
		else
			return errorCode.getMessage();
	}

}
