package ru.omsu.imit.catalog.resources;

import ru.omsu.imit.catalog.service.ProgramService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.sql.Date;

@Path("/api")
public class ProgramResource {

    private static ProgramService programService = new ProgramService();


    @POST
    @Path("/catalog/program")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addProgram(String json) {
        return programService.insertProgram(json);
    }

    @GET
    @Path("/catalog/program/{id}")
    @Produces("application/json")
    public Response getProgramById(@PathParam(value = "id") int id) {
            return programService.getById(id);
    }

    @GET
    @Path("/catalog/program/category/{id}")
    @Produces("application/json")
    public Response getProgramByCategoryID(@PathParam(value = "id") int categoryid) {
        return programService.getByCategoryId(categoryid);
    }

    @GET
    @Path("/catalog/program/developer/{id}")
    @Produces("application/json")
    public Response getProgramByDeveloperID(@PathParam(value = "id") int developerid) {
        return programService.getByDeveloperId(developerid);
    }

    @GET
    @Path("/catalog/program/creation_date/{date}")
    @Produces("application/json")
    public Response getProgramByCreationDate(@PathParam(value = "creation_date") Date date) {
        return programService.getByCreationDate(date);
    }
    @GET
    @Path("/catalog/program/last_update_date/{date}")
    @Produces("application/json")
    public Response getProgramByLastUpdateDate(@PathParam(value = "last_update_date") Date date) {
        return programService.getByLastUpdateDate(date);
    }


    @GET
    @Path("/catalog/program")
    @Produces("application/json")
    public Response getAllPrograms() {
        return programService.getAll();
    }

    @PUT
    @Path("/catalog/program/{id}")
    @Produces("application/json")
    public Response EditProgramName(@PathParam(value = "id") int id, String name) {
        return programService.editName(id, name);
    }

    @DELETE
    @Path("/catalog/program/{id}")
    @Produces("application/json")
    public Response deleteProgramByID(@PathParam(value = "id") int id, String json) {
        return programService.deleteById(id, json);
    }

}
