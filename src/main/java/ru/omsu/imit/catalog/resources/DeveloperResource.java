package ru.omsu.imit.catalog.resources;

import ru.omsu.imit.catalog.service.DeveloperService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/api")
public class DeveloperResource {

    private static DeveloperService developerService = new DeveloperService();

    @POST
    @Path("/catalog/developer")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addDeveloper(String json){
        return developerService.insertDeveloper(json);
    }

    @GET
    @Path("/catalog/developer/{id}")
    @Produces("application/json")
    public Response getById(@PathParam(value = "id") int id){
        return developerService.getById(id);
    }

    @GET
    @Path("/catalog/developer")
    @Produces("application/json")
    public Response getAll(){
        return developerService.getAll();
    }


    @DELETE
    @Path("/catalog/developer/{id}")
    @Produces("application/json")
    public Response deleteById(@PathParam(value = "id") int id) {
        return developerService.deleteById(id);
    }
}
