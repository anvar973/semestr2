package ru.omsu.imit.catalog.resources;

import ru.omsu.imit.catalog.service.CategoryService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/api")
public class CategoryResource {

    private static CategoryService categoryService = new CategoryService();

    @POST
    @Path("/catalog/category")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addCategory(String json) {
        return categoryService.insertCategory(json);
    }

    @GET
    @Path("/catalog/category/{id}")
    @Produces("application/json")
    public Response getCategoryByID(@PathParam(value = "id") int id) {
        return categoryService.getCategoryById(id);
    }

    @GET
    @Path("/catalog/category")
    @Produces("application/json")
    public Response getAllCategories() {
        return categoryService.getAllCategories();
    }

    @DELETE
    @Path("/catalog/category/{id}")
    @Produces("application/json")
    public Response deleteCategoryById(@PathParam(value = "id") int id) {
        return categoryService.deleteCategoriesById(id);
    }
}
