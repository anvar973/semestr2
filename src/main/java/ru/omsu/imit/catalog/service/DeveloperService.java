package ru.omsu.imit.catalog.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.omsu.imit.catalog.dao.DeveloperDAO;
import ru.omsu.imit.catalog.daoimpl.DeveloperDAOImpl;
import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.model.Developer;
import ru.omsu.imit.catalog.rest.request.DeveloperRequest;
import ru.omsu.imit.catalog.rest.response.DeveloperResponse;
import ru.omsu.imit.catalog.rest.response.SuccessResponse;
import ru.omsu.imit.catalog.utils.CategoryUtils;
import ru.omsu.imit.catalog.utils.ErrorCode;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;


public class DeveloperService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeveloperService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private DeveloperDAO developerDAO = new DeveloperDAOImpl();

    public Response insertDeveloper(String json) {
        LOGGER.debug(" Service Insert author " + json);
        try {
            DeveloperRequest request = CategoryUtils.getClassInstanceFromJson(GSON, json, DeveloperRequest.class);
            isAddable(request.getName(), request.getLastname());
            Developer developer = developerDAO.insert(new Developer(request.getName(), request.getLastname(), request.getPatronymic(), request.getPrograms()));
            String response = GSON.toJson(new DeveloperResponse(developer.getId(), developer.getFirstname(), developer.getLastname(), developer.getPatronymic(), developer.getPrograms()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    public Response getById(int id) {
        LOGGER.debug("Get author by ID " + id);
        try {
            Developer developer = developerDAO.getById(id);
            if (developer == null)
                throw new CatalogException(ErrorCode.ITEM_NOT_FOUND, Integer.toString(id));
            String response = GSON.toJson(new DeveloperResponse(developer.getId(), developer.getFirstname(), developer.getLastname(), developer.getPatronymic(), developer.getPrograms()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    public Response getAll() {
        LOGGER.debug("get All");
        try {
            List<Developer> developers = developerDAO.getAll();
            List<DeveloperResponse> responseList = new ArrayList<>();
            for (Developer developer : developers)
                responseList.add(new DeveloperResponse(developer.getId(), developer.getFirstname(), developer.getLastname(), developer.getPatronymic(), developer.getPrograms()));
            String respose = GSON.toJson(responseList);
            return Response.ok(respose, MediaType.APPLICATION_JSON).build();
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    public Response deleteById(int id) {
        LOGGER.debug("Delete author by ID");
        try {
            List<Developer> developers = developerDAO.getAll();
            for (Developer developer : developers)
                if (developer.getId() == id) {
                    developerDAO.deleteById(id);
                    String response = GSON.toJson(new SuccessResponse());
                    return Response.ok(response, MediaType.APPLICATION_JSON).build();
                }
                throw new CatalogException(ErrorCode.ITEM_NOT_FOUND);
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    private void isAddable(String firstname, String lastname) throws CatalogException {
        if (firstname == null || firstname.trim().equals(""))
            throw new CatalogException(ErrorCode.NULL_FIELD);
        if (lastname == null || lastname.trim().equals(""))
            throw new CatalogException(ErrorCode.NULL_FIELD);
    }

}
