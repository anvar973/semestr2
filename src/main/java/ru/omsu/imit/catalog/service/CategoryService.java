package ru.omsu.imit.catalog.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.catalog.dao.CategoryDAO;
import ru.omsu.imit.catalog.daoimpl.CategoryDAOImpl;
import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.model.Category;
import ru.omsu.imit.catalog.rest.request.CategoryRequest;
import ru.omsu.imit.catalog.rest.response.CategoryResponse;
import ru.omsu.imit.catalog.rest.response.SuccessResponse;
import ru.omsu.imit.catalog.utils.CategoryUtils;
import ru.omsu.imit.catalog.utils.ErrorCode;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashSet;
import java.util.Set;

public class CategoryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private CategoryDAO categoryDAO = new CategoryDAOImpl();

    public Response insertCategory(String json) {
        LOGGER.debug("Insert section " + json);
        try {
            CategoryRequest request = CategoryUtils.getClassInstanceFromJson(GSON, json, CategoryRequest.class);
            isAddable(request);
            Category category = new Category(request.getName(), request.getPrograms());
            Category addCategory = categoryDAO.insert(category);
            String response = GSON.toJson(new CategoryResponse(addCategory.getId(), addCategory.getName(), addCategory.getPrograms()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    public Response getCategoryById(int id) {
        LOGGER.debug("Get section by ID " + id);
        try {
            Category category = categoryDAO.getById(id);
            if (category == null)
                throw new CatalogException(ErrorCode.ITEM_NOT_FOUND, Integer.toString(id));
            String response = GSON.toJson(new CategoryResponse(category.getId(), category.getName(), category.getPrograms()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    public Response getAllCategories() {
        LOGGER.debug("get All categories");
        Set<Category> categories = categoryDAO.getAll();
        Set<CategoryResponse> responseList = new HashSet<>();
        for (Category category : categories)
            responseList.add(new CategoryResponse(category.getId(), category.getName(), category.getPrograms()));
        String response = GSON.toJson(responseList);
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response deleteCategoriesById(int id) {
        LOGGER.debug("Delete section by ID");
        try {
            Set<Category> categories = categoryDAO.getAll();
            for (Category category : categories)
                if (category.getId() == id) {
                    categoryDAO.deleteById(id);
                    String response = GSON.toJson(new SuccessResponse());
                    return Response.ok(response, MediaType.APPLICATION_JSON).build();
                }
            throw new CatalogException(ErrorCode.ITEM_NOT_FOUND);
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    private void isAddable(CategoryRequest request) throws CatalogException {
        String name = request.getName();
        if (name == null || name.trim().equals(""))
            throw new CatalogException(ErrorCode.NULL_FIELD);
        Set<Category> categories = categoryDAO.getAll();
        for (Category category : categories)
            if (category.getName().equals(name))
                throw new CatalogException(ErrorCode.DUPLICATED_ITEM);
    }

}
