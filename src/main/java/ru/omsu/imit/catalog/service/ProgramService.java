package ru.omsu.imit.catalog.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.omsu.imit.catalog.dao.ProgramDAO;
import ru.omsu.imit.catalog.dao.DeveloperDAO;
import ru.omsu.imit.catalog.dao.CategoryDAO;
import ru.omsu.imit.catalog.daoimpl.ProgramDAOImpl;
import ru.omsu.imit.catalog.daoimpl.DeveloperDAOImpl;
import ru.omsu.imit.catalog.daoimpl.CategoryDAOImpl;
import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.model.Category;
import ru.omsu.imit.catalog.model.Program;
import ru.omsu.imit.catalog.model.Developer;
import ru.omsu.imit.catalog.rest.request.ProgramRequest;
import ru.omsu.imit.catalog.rest.response.ProgramResponse;
import ru.omsu.imit.catalog.rest.response.SuccessResponse;
import ru.omsu.imit.catalog.utils.CategoryUtils;
import ru.omsu.imit.catalog.utils.ErrorCode;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ProgramService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProgramService.class);
    private static final Gson GSON = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
    private ProgramDAO programDAO = new ProgramDAOImpl();
    private CategoryDAO categoryDAO = new CategoryDAOImpl();
    private DeveloperDAO developerDAO = new DeveloperDAOImpl();

    public Response insertProgram(String json) {
        LOGGER.debug("Insert announcement " + json);
        try {
            ProgramRequest request = CategoryUtils.getClassInstanceFromJson(GSON, json, ProgramRequest.class);
            isAddable(request);
            Program program = programDAO.insert(new Program(request.getName(), request.getCreationDate(),
                    request.getLast_update_date(), request.getCategoryid(), request.getDeveloperid()));
            String response = GSON.toJson(new ProgramResponse(program.getId(), program.getTitle(),
                    program.getContent(), program.getAn_date(), program.getSectionId(), program.getAuthorId()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    public Response getById(int id) {
        LOGGER.debug("Get announcement by ID " + id);
        try {
            Program program = programDAO.getById(id);
            if (program == null)
                throw new CatalogException(ErrorCode.ITEM_NOT_FOUND, Integer.toString(id));
            String response = GSON.toJson(new ProgramResponse(program.getId(), program.getTitle(), program.getContent(), program.getAn_date(), program.getSectionId(), program.getAuthorId()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    public Response getByCategoryId(int sectionId) {
        LOGGER.debug("Get announcement by Category's ID " + sectionId);
        try {
            Set<Category> categories = categoryDAO.getAll();
            for (Category category : categories)
                if (category.getId() == sectionId) {
                    Set<Program> programs = programDAO.getByCategoryId(sectionId);
                    Set<ProgramResponse> responseSet = new HashSet<>();
                    for (Program program : programs)
                        responseSet.add(new ProgramResponse(program.getId(), program.getTitle(), program.getContent(), program.getAn_date(), program.getSectionId(), program.getAuthorId()));
                    String response = GSON.toJson(responseSet);
                    return Response.ok(response, MediaType.APPLICATION_JSON).build();
                }
            throw new CatalogException(ErrorCode.ITEM_NOT_FOUND, " by Category ID " + Integer.toString(sectionId));
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    public Response getByDeveloperId(int authorId) {
        LOGGER.debug("Get announcement by Developer's ID " + authorId);
        try {
            List<Developer> developers = developerDAO.getAll();
            for (Developer developer : developers)
                if (developer.getId() == authorId) {
                    Set<Program> programs = programDAO.getByDeveloperId(authorId);
                    Set<ProgramResponse> responseSet = new HashSet<>();
                    for (Program program : programs)
                        responseSet.add(new ProgramResponse(program.getId(), program.getTitle(), program.getContent(), program.getAn_date(), program.getSectionId(), program.getAuthorId()));
                    String response = GSON.toJson(responseSet);
                    return Response.ok(response, MediaType.APPLICATION_JSON).build();
                }
            throw new CatalogException(ErrorCode.ITEM_NOT_FOUND, " by Developer ID " + Integer.toString(authorId));
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    public Response getByCreationDate(Date date) {
        LOGGER.debug("Get announcement by Date " + date);
        try {
            if (!(date.toString().trim().equals("") || date.toString() == null)) {
                Set<Program> programs = programDAO.getByCreationDate(date);
                Set<ProgramResponse> responseSet = new HashSet<>();
                for (Program program : programs)
                    responseSet.add(new ProgramResponse(program.getId(), program.getTitle(), program.getContent(), program.getAn_date(), program.getSectionId(), program.getAuthorId()));
                String response = GSON.toJson(responseSet);
                return Response.ok(response, MediaType.APPLICATION_JSON).build();
            }
            throw new CatalogException(ErrorCode.UNKNOWN_ERROR, " Wrong parameter");
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    public Response getByLastUpdateDate(Date date) {
        LOGGER.debug("Get announcement by Date " + date);
        try {
            if (!(date.toString().trim().equals("") || date.toString() == null)) {
                Set<Program> programs = programDAO.getByCreationDate(date);
                Set<ProgramResponse> responseSet = new HashSet<>();
                for (Program program : programs)
                    responseSet.add(new ProgramResponse(program.getId(), program.getTitle(), program.getContent(), program.getAn_date(), program.getSectionId(), program.getAuthorId()));
                String response = GSON.toJson(responseSet);
                return Response.ok(response, MediaType.APPLICATION_JSON).build();
            }
            throw new CatalogException(ErrorCode.UNKNOWN_ERROR, " Wrong parameter");
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    public Response getAll() {
        LOGGER.debug("get All programs");
        Set<Program> programs = programDAO.getAll();
        Set<ProgramResponse> responseSet = new HashSet<>();
        for (Program program : programs)
            responseSet.add(new ProgramResponse(program.getId(), program.getTitle(), program.getContent(), program.getAn_date(), program.getSectionId(), program.getAuthorId()));
        String response = GSON.toJson(responseSet);
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response editName(int id, String json) {
        LOGGER.debug("Edit Program's content by ID " + id);
        try {
            ProgramRequest request = CategoryUtils.getClassInstanceFromJson(GSON, json, ProgramRequest.class);
            Program program = programDAO.editProgram(id, json);
            String response = GSON.toJson(new ProgramResponse(program.getId(), program.getTitle(),
                    program.getContent(), program.getAn_date(), program.getSectionId(), program.getAuthorId()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }


    public Response deleteById(int id, String json) {
        LOGGER.debug("Delete announcement by ID");
        try {
            programDAO.deleteById(id);
            String response = GSON.toJson(new SuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (CatalogException e) {
            return CategoryUtils.failureResponse(e);
        }
    }

    private void isAddable(ProgramRequest request) throws CatalogException {
        String title = request.getName();
        String content = request.getCreationDate();
        Date an_date = request.getLast_update_date();
        int sectionId = request.getCategoryid();
        int authorId = request.getDeveloperid();

        if (title == null || title.trim().equals(""))
            throw new CatalogException(ErrorCode.NULL_FIELD);
        if (content == null || content.trim().equals(""))
            throw new CatalogException(ErrorCode.NULL_FIELD);
        if (an_date == null)
            throw new CatalogException(ErrorCode.NULL_FIELD);
        if (sectionId == 0 || authorId == 0)
            throw new CatalogException(ErrorCode.NULL_FIELD);
    }
}
