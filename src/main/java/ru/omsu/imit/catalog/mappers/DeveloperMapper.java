package ru.omsu.imit.catalog.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.catalog.model.Developer;

import java.util.List;
import java.util.Set;

public interface DeveloperMapper {


    @Insert("INSERT INTO developer (name) VALUE " +
            "( #{name})")
    @Options(useGeneratedKeys = true)
    public Integer insert(Developer developer);

    @Select("SELECT id, name FROM developer WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "program", column = "id", javaType = Set.class, many = @Many(select = "ProgramMapper.getByDeveloperId", fetchType = FetchType.LAZY))})
    public Developer getById(int id);


    @Select("SELECT id, name FROM developer")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "program", column = "id", javaType = Set.class, many = @Many(select = "ProgramMapper.getByDeveloperId", fetchType = FetchType.LAZY))})
    public List<Developer> getAll();

    @Select("SELECT id, name FROM developer WHERE id IN (SELECT developerid FROM program WHERE id = #{id})")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "program", column = "id", javaType = Set.class, many = @Many(select = "ProgramMapper.getByDeveloperId", fetchType = FetchType.LAZY))})
    public List<Developer> getByProgram(int programid);

    @Delete("DELETE FROM developer WHERE id = #{id}")
    public void delete(@Param("id") int id);

    @Delete("DELETE FROM developer")
    public void deleteAll();
}
