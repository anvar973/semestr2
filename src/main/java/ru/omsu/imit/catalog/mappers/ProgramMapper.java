package ru.omsu.imit.catalog.mappers;

import org.apache.ibatis.annotations.*;
import ru.omsu.imit.catalog.model.Program;

import java.sql.Date;
import java.util.Set;

public interface ProgramMapper {

    @Insert("INSERT INTO program (name, creation_date, last_update_date, categoryid, developerid) VALUES" +
            "( #{name}, #{creation_date}, #{last_update_date}, #{categoryid}, #{developerid} )")
    @Options(useGeneratedKeys = true)
    public Integer insert(Program program);

    @Select("SELECT id, name, creation_date, last_update_date FROM program WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id")
    })
    public Program getById(int id);

    @Select("SELECT id, name, creation_date, last_update_date FROM program;")
    public Set<Program> getAll();

    @Select("SELECT id, name, creation_date, last_update_date FROM program WHERE categoryid = #{category.id}")
    public Set<Program> getByCategoryId(@Param("categoryid") int categoryid);

    @Select("SELECT id, name, creation_date, last_update_date FROM program WHERE developerid = #{developer.id}")
    public Set<Program> getByDeveloperId(@Param("developerid") int developerid);

    @Select("SELECT id, name, creation_date, last_update_date FROM program WHERE creation_date = #{creation_date}")
    public Set<Program> getByCreationDate(@Param("creation_date") Date creation_date);

    @Select("SELECT id, name, cr_date, last_update_date FROM program WHERE last_update_date = #{last_update_date}")
    public Set<Program> getByLastUpdateDate(@Param("last_update_date") Date last_update_date);

    @Update("UPDATE program SET name = #{name} WHERE id = #{id} ")
    public void editContent(@Param("id") int id, @Param("name") String content);

    @Delete("DELETE FROM program WHERE id = #{program.id}")
    public void delete(@Param("program") Program program);

    @Delete("DELETE FROM program")
    public void deleteAll();
}
