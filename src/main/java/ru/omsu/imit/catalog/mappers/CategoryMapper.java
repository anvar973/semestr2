package ru.omsu.imit.catalog.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.catalog.model.Category;

import java.util.Set;

public interface CategoryMapper {

    @Insert("INSERT INTO category (name) VALUES " +
            "( #{name})")
    @Options(useGeneratedKeys = true)
    public Integer insert(Category category);

    @Select("SELECT id, name FROM category WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "program", column = "id", javaType = Set.class, many = @Many(select = "ProgramMapper.getByDeveloperId", fetchType = FetchType.LAZY))})
    public Category getById(int id);

    @Select("SELECT id, name FROM category")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "program", column = "id", javaType = Set.class, many = @Many(select = "ProgramMapper.getByDeveloperId", fetchType = FetchType.LAZY))})
    public Set<Category> getAll();

    @Delete("DELETE FROM category WHERE id = #{categoryid}")
    public void delete(@Param("categoryid") int categoryid);

    @Delete("DELETE FROM category")
    public void deleteAll();
}
