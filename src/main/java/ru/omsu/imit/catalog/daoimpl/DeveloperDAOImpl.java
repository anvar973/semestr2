package ru.omsu.imit.catalog.daoimpl;


import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.catalog.dao.DeveloperDAO;
import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.model.Developer;
import ru.omsu.imit.catalog.utils.ErrorCode;

import java.util.List;

public class DeveloperDAOImpl extends BaseDAOImpl implements DeveloperDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeveloperDAOImpl.class);

    @Override
    public Developer insert(Developer developer) throws CatalogException {
        LOGGER.debug(" DAO Insert Developer {}", developer);
        try (SqlSession sqlSession = getSession()) {
            try {
                getDeveloperMapper(sqlSession).insert(developer);
            } catch (RuntimeException e) {
                LOGGER.debug("Can not insert Developer {}, {}", developer, e);
                sqlSession.rollback();
                throw new CatalogException(ErrorCode.UNKNOWN_ERROR);
            }
            sqlSession.commit();
        }
        return developer;
    }

    @Override
    public Developer getById(int id) throws CatalogException {
        try (SqlSession sqlSession = getSession()) {
            return getDeveloperMapper(sqlSession).getById(id);
        } catch (RuntimeException e) {
            LOGGER.debug("Can not get Developer {}", e);
            throw new CatalogException(ErrorCode.UNKNOWN_ERROR);
        }
    }

    @Override
    public List<Developer> getAll() throws CatalogException {
        try (SqlSession sqlSession = getSession()) {
            return getDeveloperMapper(sqlSession).getAll();
        } catch (RuntimeException e) {
            LOGGER.debug("Can not get all Developers {}", e);
            throw new CatalogException(ErrorCode.ITEM_NOT_FOUND);
        }
    }

    @Override
    public List<Developer> getByProgramId(int announcementId) {
        try (SqlSession sqlSession = getSession()) {
            return getDeveloperMapper(sqlSession).getByProgram(announcementId);
        } catch (RuntimeException e) {
            LOGGER.debug("Can not get Developer {}", e);
            throw e;
        }
    }

    @Override
    public void deleteById(int id) {
        LOGGER.debug("Delete developer by ID {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                getDeveloperMapper(sqlSession).delete(id);
            } catch (RuntimeException e) {
                LOGGER.debug("Can not deleteById developer by ID {}", e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("Delete all Developers");
        try (SqlSession sqlSession = getSession()) {
            try {
                getDeveloperMapper(sqlSession).deleteAll();
            } catch (RuntimeException e) {
                LOGGER.debug("Can not deleteById all Developers {}", e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }
}
