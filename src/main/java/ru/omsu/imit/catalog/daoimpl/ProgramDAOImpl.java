package ru.omsu.imit.catalog.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.catalog.dao.ProgramDAO;
import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.model.Program;

import java.sql.Date;
import java.util.Set;

public class ProgramDAOImpl extends BaseDAOImpl implements ProgramDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProgramDAOImpl.class);

    @Override
    public Program insert(Program program) {
        LOGGER.debug("Insert Program {}", program);
        try (SqlSession sqlSession = getSession()) {
            try {
                getProgramMapper(sqlSession).insert(program);
            } catch (RuntimeException e) {
                LOGGER.debug("Can not insert Program {}, {}", program, e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
        return program;
    }

    @Override
    public Program getById(int id) {
        try (SqlSession sqlSession = getSession()) {
            return getProgramMapper(sqlSession).getById(id);
        } catch (RuntimeException e) {
            LOGGER.debug("Can not get Program {}", e);
            throw e;
        }
    }

    @Override
    public Set<Program> getAll() {
        try (SqlSession sqlSession = getSession()) {
            return getProgramMapper(sqlSession).getAll();
        } catch (RuntimeException e) {
            LOGGER.debug("Can not get all Programs {}", e);
            throw e;
        }
    }

    @Override
    public Set<Program> getByCategoryId(int categoryId) {
        try (SqlSession sqlSession = getSession()) {
            return getProgramMapper(sqlSession).getByCategoryId(categoryId);
        } catch (RuntimeException e) {
            LOGGER.debug("Can not get Program {}", e);
            throw e;
        }
    }

    @Override
    public Set<Program> getByDeveloperId(int developerId) {
        try (SqlSession sqlSession = getSession()) {
            return getProgramMapper(sqlSession).getByDeveloperId(developerId);
        } catch (RuntimeException e) {
            LOGGER.debug("Can not get Program {}", e);
            throw e;
        }
    }

    public Set<Program> getByCreationDate(Date date) {
        try (SqlSession sqlSession = getSession()) {
            return getProgramMapper(sqlSession).getByCreationDate(date);
        } catch (RuntimeException e) {
            LOGGER.debug("Can not get Program {}", e);
            throw e;
        }
    }

    @Override
    public Set<Program> getByLastUpdateDate(Date date) throws CatalogException {
        try (SqlSession sqlSession = getSession()) {
            return getProgramMapper(sqlSession).getByLastUpdateDate(date);
        } catch (RuntimeException e) {
            LOGGER.debug("Can not get Program {}", e);
            throw e;
        }
    }

    @Override
    public Program editProgram(int id, String name) {
        LOGGER.debug("Edit content {}", name);
        try (SqlSession sqlSession = getSession()) {
            try {
                getProgramMapper(sqlSession).editContent(id, name);
            } catch (RuntimeException e) {
                LOGGER.debug("Can not change Program's content {}", e);
                throw e;
            }
            sqlSession.commit();
        }
        return getById(id);
    }

    @Override
    public void deleteById(int programId) {

        LOGGER.debug("Delete programs by ID {}", programId);
        try (SqlSession sqlSession = getSession()) {
            try {
                getDeveloperMapper(sqlSession).delete(programId);
            } catch (RuntimeException e) {
                LOGGER.debug("Can not deleteById programs by ID {}", e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("Delete all Programs");
        try (SqlSession sqlSession = getSession()) {
            try {
                getProgramMapper(sqlSession).deleteAll();
            } catch (RuntimeException e) {
                LOGGER.debug("Can not deleteById all programs {}", e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }

}
