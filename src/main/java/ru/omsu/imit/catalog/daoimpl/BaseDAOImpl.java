package ru.omsu.imit.catalog.daoimpl;

import org.apache.ibatis.session.SqlSession;
import ru.omsu.imit.catalog.mappers.ProgramMapper;
import ru.omsu.imit.catalog.mappers.DeveloperMapper;
import ru.omsu.imit.catalog.mappers.CategoryMapper;
import ru.omsu.imit.catalog.utils.MyBatisUtils;

public class BaseDAOImpl {

    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected DeveloperMapper getDeveloperMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(DeveloperMapper.class);
    }

    protected CategoryMapper getCategoryMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(CategoryMapper.class);
    }

    protected ProgramMapper getProgramMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(ProgramMapper.class);
    }
}
