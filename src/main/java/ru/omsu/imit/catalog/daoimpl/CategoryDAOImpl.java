package ru.omsu.imit.catalog.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.catalog.dao.CategoryDAO;
import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.model.Category;
import ru.omsu.imit.catalog.utils.ErrorCode;

import java.util.Set;

public class CategoryDAOImpl extends BaseDAOImpl implements CategoryDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeveloperDAOImpl.class);

    @Override
    public Category insert(Category category) throws CatalogException {
        LOGGER.debug("Insert Category {}", category);
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).insert(category);
            } catch (RuntimeException e) {
                LOGGER.debug("Can not insert Category {}, {}", category, e);
                sqlSession.rollback();
                throw new CatalogException(ErrorCode.UNKNOWN_ERROR);
            }
            sqlSession.commit();
        }
        return category;
    }

    @Override
    public Category getById(int id) {
        try (SqlSession sqlSession = getSession()) {
            return getCategoryMapper(sqlSession).getById(id);
        } catch (RuntimeException e) {
            LOGGER.debug("Can not get Category {}", e);
            throw e;
        }
    }

    @Override
    public Set<Category> getAll() {
        try (SqlSession sqlSession = getSession()) {
            return getCategoryMapper(sqlSession).getAll();
        } catch (RuntimeException e) {
            LOGGER.debug("Can not get all Categories {}", e);
            throw e;
        }
    }

    @Override
    public void deleteById(int sectionId) {
        LOGGER.debug("Delete category by ID {}", sectionId);
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).delete(sectionId);
            } catch (RuntimeException e) {
                LOGGER.debug("Can not delete category by ID {}", e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("Delete all Categories");
        try (SqlSession sqlSession = getSession()) {
            try {
               getCategoryMapper(sqlSession).deleteAll();
            } catch (RuntimeException e) {
                LOGGER.debug("Can not deleteById all Categories {}", e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }
}
