package ru.omsu.imit.catalog.model;

import java.sql.Date;
import java.util.Objects;

public class Program {
    private int id;
    private String name;
    private Date creation_date;
    private Date last_update_date;
    private int categoryId;
    private int developerId;


    public Program(int id, String name, Date creation_date, Date last_update_date, int categoryId, int developerId) {
        super();
        this.id = id;
        this.name = name;
        this.creation_date = creation_date;
        this.last_update_date = last_update_date;
        this.categoryId = categoryId;
        this.developerId = developerId;
    }

    public Program(String name, Date creation_date, Date last_update_date, int categoryId, int developerId) {
        this(0, name, creation_date, last_update_date, categoryId, developerId);
    }


    public Program() {

    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(Date last_update_date) {
        this.last_update_date = last_update_date;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getDeveloperId() {
        return developerId;
    }

    public void setDeveloperId(int developerId) {
        this.developerId = developerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Program program = (Program) o;
        return id == program.id &&
                categoryId == program.categoryId &&
                developerId == program.developerId &&
                Objects.equals(name, program.name) &&
                Objects.equals(creation_date, program.creation_date) &&
                Objects.equals(last_update_date, program.last_update_date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, creation_date, last_update_date, categoryId, developerId);
    }

    @Override
    public String toString() {
        return "Program{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", creation_date=" + creation_date +
                ", last_update_date=" + last_update_date +
                ", categoryId=" + categoryId +
                ", developerId=" + developerId +
                '}';
    }
}
