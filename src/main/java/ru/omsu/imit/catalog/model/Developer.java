package ru.omsu.imit.catalog.model;


import java.util.HashSet;
import java.util.Set;

public class Developer {
    private int id;
    private String name;
    private Set<Program> programs;

    public Developer(int id, String name,Set<Program> programs) {
        super();
        this.id = id;
        this.name = name;
        this.programs = programs;
    }

    public Developer(int id, String name){
        this(id,name, new HashSet<>());
    }

    public Developer(String name, Set<Program> programs) {
        this(0, name, programs);
    }

    public Developer(String name) {
        this(0, name, new HashSet<>());
    }

    public Developer(){
    }

    public Set<Program> getPrograms() {
        return programs;
    }

    public void setPrograms(Set<Program> programs) {
        this.programs = programs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Developer [id=" + id + ", name=" + name + ", programs=" + programs;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        Developer developer = (Developer) o;

        if (id != developer.id) return false;
        if (!name.equals(developer.name)) return false;
        return programs.equals(developer.programs);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (programs != null ? programs.hashCode() : 0);
        return result;
    }
}
