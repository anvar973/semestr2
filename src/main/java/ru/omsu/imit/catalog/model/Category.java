package ru.omsu.imit.catalog.model;

import java.util.HashSet;
import java.util.Set;

public class Category {
    private int id;
    private String name;
    private Set<Program> programs;

    public Category(int id, String name, Set<Program> programs) {
        super();
        this.id = id;
        this.name = name;
        this.programs = programs;
    }

    public Category(String name, Set<Program> programs) {
        this(0, name, programs);
    }

    public Category(String name) {
        this(0, name, new HashSet<>());
    }

    public Category() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Program> getPrograms() {
        return programs;
    }

    public void setPrograms(Set<Program> programs) {
        this.programs = programs;
    }

    @Override
    public String toString() {
        return "Category[id=" + id + ", name=" + name + ", programs=" + programs + "]";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (id != category.id) return false;
        if (!name.equals(category.name)) return false;
        return programs != null ? programs.equals(category.programs) : category.programs == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + (programs != null ? programs.hashCode() : 0);
        return result;
    }
}
