package ru.omsu.imit.catalog.utils;


public enum ErrorCode {

	SUCCESS("", ""),
	ITEM_NOT_FOUND("item", "Item %s not found"),
	DUPLICATED_ITEM("item", "This item can't be duplicated. It must be unique."),
	NULL_FIELD("field", "Field can't be null"),
	NULL_REQUEST("json", "Null request"),
	JSON_PARSE_EXCEPTION("json", "Json parse exception :  %s"),
	WRONG_URL("url", "Wrong URL"),
	METHOD_NOT_ALLOWED("url", "Method not allowed"),
	UNKNOWN_ERROR("error", "Unknown error");

	private String field;
	private String message;

	private ErrorCode(String field, String message) {
		this.field = field;
		this.message = message;
	}

	public String getField() {
		return field;
	}

	public String getMessage() {
		return message;
	}


}
