package ru.omsu.imit.catalog.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang.StringUtils;
import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.rest.response.FailureResponse;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

//DbSqlEditor:LastDefaultSchema=catalog
//		lastConnected=1525238927
//		serverVersion=5.7.20-log



public class CategoryUtils {

	private static final Gson GSON = new Gson();

	public static <T> T getClassInstanceFromJson(Gson gson, String json, Class<T> clazz) throws CatalogException {
		if (StringUtils.isEmpty(json)) {
			throw new CatalogException(ErrorCode.NULL_REQUEST);
		}
		try {
			return gson.fromJson(json, clazz);
		} catch (JsonSyntaxException ex) {
			throw new CatalogException(ErrorCode.JSON_PARSE_EXCEPTION, json);
		}
	}
	
	public static Response failureResponse(Status status, CatalogException ex) {
		return Response.status(status).entity(GSON.toJson(new FailureResponse(ex.getErrorCode(), ex.getMessage()))).build();
	}
	
	public static Response failureResponse(CatalogException ex) {
		return failureResponse(Status.BAD_REQUEST, ex);
	}

}
