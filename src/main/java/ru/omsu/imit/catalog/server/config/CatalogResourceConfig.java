package ru.omsu.imit.catalog.server.config;

import org.glassfish.jersey.server.ResourceConfig;

public class CatalogResourceConfig extends ResourceConfig {
    public CatalogResourceConfig() {
        packages("ru.omsu.imit.board.resources",
                "ru.omsu.imit.board.rest.mappers");
    }
}
