package ru.omsu.imit.catalog.dao;


import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.model.Program;

import java.sql.Date;
import java.util.Set;

public interface ProgramDAO {
    public Program insert(Program program);

    public Program getById(int id) throws CatalogException;

    public Set<Program> getAll();

    public Set<Program> getByCategoryId(int categoryId) throws CatalogException;

    public Set<Program> getByDeveloperId(int developerId) throws CatalogException;

    public Set<Program> getByCreationDate(Date date) throws CatalogException;

    public Set<Program> getByLastUpdateDate(Date date) throws CatalogException;

    public Program editProgram(int id, String title) throws CatalogException;

    public void deleteById(int programId) throws CatalogException;

    public void deleteAll();


}
