package ru.omsu.imit.catalog.dao;


import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.model.Category;

import java.util.Set;

public interface CategoryDAO {

    public Category insert(Category category) throws CatalogException;

    public Category getById(int id) throws CatalogException;

    public Set<Category> getAll();

    public void deleteById(int categoryId) throws CatalogException;

    public void deleteAll();

}
