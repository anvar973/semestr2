package ru.omsu.imit.catalog.dao;


import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.model.Developer;

import java.util.List;

public interface DeveloperDAO {
    public Developer insert(Developer developer) throws CatalogException;

    public Developer getById(int id) throws CatalogException;

    public List<Developer> getAll() throws CatalogException;

    public  List<Developer> getByProgramId(int programId) throws CatalogException;

    public void deleteById(int developerId) throws CatalogException;

    public void deleteAll();
}
