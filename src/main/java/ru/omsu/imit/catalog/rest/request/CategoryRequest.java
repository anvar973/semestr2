package ru.omsu.imit.catalog.rest.request;

import ru.omsu.imit.catalog.model.Program;

import java.util.HashSet;
import java.util.Set;

public class CategoryRequest {
    private String name;
    private Set<Program> programs;

    public CategoryRequest(String name, Set<Program> programs) {
        super();
        this.name = name;
        this.programs = programs;
    }

    public CategoryRequest(String name) {
        this(name, new HashSet<>());
    }

    public String getName() {
        return name;
    }

    public Set<Program> getPrograms() {
        return programs;
    }

}
