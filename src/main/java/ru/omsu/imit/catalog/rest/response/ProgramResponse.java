package ru.omsu.imit.catalog.rest.response;

import java.sql.Date;

public class ProgramResponse {
    private int id;
    private String name;
    private Date creation_date;
    private Date last_update_date;
    private int categoryId, developerId;

    public ProgramResponse(int id, String name, Date creation_date, Date last_update_date, int categoryId, int developerId) {
        this.id = id;
        this.name = name;
        this.creation_date = creation_date;
        this.last_update_date = last_update_date;
        this.categoryId = categoryId;
        this.developerId = developerId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public Date getLast_update_date() {
        return last_update_date;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public int getDeveloperId() {
        return developerId;
    }
}
