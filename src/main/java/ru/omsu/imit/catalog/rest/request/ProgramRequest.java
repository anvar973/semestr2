package ru.omsu.imit.catalog.rest.request;

import java.sql.Date;

public class ProgramRequest {
    private String name;
    private Date creation_date;
    private Date last_update_date;
    private int categoryid;
    private int developerid;

    public ProgramRequest(String name, Date creation_date, Date last_update_date, int categoryid, int developerid) {
        this.name = name;
        this.creation_date = creation_date;
        this.last_update_date = last_update_date;
        this.categoryid = categoryid;
        this.developerid = developerid;
    }

    public ProgramRequest(String name, String creation_date, String last_update_date, int categoryid, int developerid) {
        this.name = name;
        this.creation_date = Date.valueOf(creation_date);
        this.last_update_date = Date.valueOf(last_update_date);
        this.categoryid = categoryid;
        this.developerid = developerid;
    }

    public String getName() {
        return name;
    }

    public Date getCreationDate() {
        return creation_date;
    }

    public Date getLast_update_date() {
        return last_update_date;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public int getDeveloperid() {
        return developerid;
    }
}
