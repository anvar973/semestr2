package ru.omsu.imit.catalog.rest.mappers;

import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.utils.ErrorCode;
import ru.omsu.imit.catalog.utils.CategoryUtils;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class WrongURLExceptionMapper implements	ExceptionMapper<NotFoundException> {

    @Override
	public Response toResponse(NotFoundException exception) {
		return CategoryUtils.failureResponse(Status.NOT_FOUND, new CatalogException(ErrorCode.WRONG_URL));
	}
}