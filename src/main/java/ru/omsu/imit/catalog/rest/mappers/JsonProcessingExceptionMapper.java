package ru.omsu.imit.catalog.rest.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import ru.omsu.imit.catalog.utils.ErrorCode;
import ru.omsu.imit.catalog.utils.CategoryUtils;
import ru.omsu.imit.catalog.exception.CatalogException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class JsonProcessingExceptionMapper implements
		ExceptionMapper<JsonProcessingException> {
	@Override
    public Response toResponse(JsonProcessingException exception) {
		return CategoryUtils.failureResponse(Status.BAD_REQUEST, new CatalogException(ErrorCode.JSON_PARSE_EXCEPTION));
    }
}