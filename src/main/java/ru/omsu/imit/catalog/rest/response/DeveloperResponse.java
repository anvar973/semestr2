package ru.omsu.imit.catalog.rest.response;

import ru.omsu.imit.catalog.model.Program;

import java.util.HashSet;
import java.util.Set;

public class DeveloperResponse {

    private int id;
    private String name;
    private Set<Program> programs;

    public DeveloperResponse(int id, String name, Set<Program> announcments) {
        this.id = id;
        this.name = name;
        this.programs = announcments;
    }

    public DeveloperResponse(int id, String name) {
        this(id, name, new HashSet<>());
    }

    public int getId() {
        return id;
    }

    public Set<Program> getPrograms() {
        return programs;
    }

    public String getName() {
        return name;
    }

}
