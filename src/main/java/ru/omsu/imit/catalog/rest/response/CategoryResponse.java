package ru.omsu.imit.catalog.rest.response;

import ru.omsu.imit.catalog.model.Program;

import java.util.HashSet;
import java.util.Set;

public class CategoryResponse {
    private int id;
    private String name;
    private Set<Program> programs;

    public CategoryResponse(int id, String name, Set<Program> programs) {
        super();
        this.id = id;
        this.name = name;
        this.programs = programs;
    }

    public CategoryResponse(int id, String name) {
        this(id, name, new HashSet<>());
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Program> getPrograms() {
        return programs;
    }

}
