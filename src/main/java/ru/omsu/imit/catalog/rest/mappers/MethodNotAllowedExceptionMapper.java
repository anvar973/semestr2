package ru.omsu.imit.catalog.rest.mappers;


import ru.omsu.imit.catalog.exception.CatalogException;
import ru.omsu.imit.catalog.utils.ErrorCode;
import ru.omsu.imit.catalog.utils.CategoryUtils;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class MethodNotAllowedExceptionMapper implements	ExceptionMapper<NotAllowedException> {

    @Override
	public Response toResponse(NotAllowedException exception) {
		return CategoryUtils.failureResponse(Status.METHOD_NOT_ALLOWED, new CatalogException(ErrorCode.METHOD_NOT_ALLOWED));
	}
}