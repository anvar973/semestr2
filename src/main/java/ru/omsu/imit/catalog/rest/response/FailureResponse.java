package ru.omsu.imit.catalog.rest.response;

import ru.omsu.imit.catalog.utils.ErrorCode;

public class FailureResponse {

	private ErrorCode errorCode;
	private String message;

	public FailureResponse(ErrorCode errorCode, String message) {
		super();
		this.errorCode = errorCode;
		this.message = message;
	}

	public FailureResponse(ErrorCode errorCode) {
		this(errorCode, "");
	}
	public ErrorCode getErrorCode() {
		return errorCode;
	}


	public String getMessage() {
		return message;
	}


}
