package ru.omsu.imit.catalog.rest.request;

import ru.omsu.imit.catalog.model.Program;

import java.util.HashSet;
import java.util.Set;

public class DeveloperRequest {

    private String name;
    private Set<Program> programs;

    public DeveloperRequest(String name, Set<Program> announcments) {
        super();
        this.name = name;
        this.programs = announcments;
    }

    public DeveloperRequest(String name) {
        this.name = name;
        this.programs = new HashSet<>();
    }

    public Set<Program> getPrograms() {
        return programs;
    }

    public String getName() {
        return name;
    }

}