package ru.omsu.imit.todolist.dao;

import java.util.List;

import ru.omsu.imit.todolist.exception.TodoListException;
import ru.omsu.imit.todolist.model.TodoItem;


public interface TodoDAO {

	public TodoItem addItem(TodoItem item);
	public TodoItem getById(int id) throws TodoListException;
	public List<TodoItem> getAll();
	public TodoItem editItem(int id, String newText) throws TodoListException;
	public void deleteById(int id) throws TodoListException;


}