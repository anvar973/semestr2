package ru.omsu.imit.todolist.rest.mappers;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import ru.omsu.imit.todolist.exception.TodoListException;
import ru.omsu.imit.todolist.utils.ErrorCode;
import ru.omsu.imit.todolist.utils.TodoLIstUtils;


@Provider
public class WrongURLExceptionMapper implements	ExceptionMapper<NotFoundException> {

    @Override
	public Response toResponse(NotFoundException exception) {
		return TodoLIstUtils.failureResponse(Status.NOT_FOUND, new TodoListException(ErrorCode.WRONG_URL));
	}
}