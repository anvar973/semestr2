package ru.omsu.imit.todolist.rest.mappers;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import ru.omsu.imit.todolist.exception.TodoListException;
import ru.omsu.imit.todolist.utils.ErrorCode;
import ru.omsu.imit.todolist.utils.TodoLIstUtils;


@Provider
public class MethodNotAllowedExceptionMapper implements	ExceptionMapper<NotAllowedException> {

    @Override
	public Response toResponse(NotAllowedException exception) {
		return TodoLIstUtils.failureResponse(Status.METHOD_NOT_ALLOWED, new TodoListException(ErrorCode.METHOD_NOT_ALLOWED));
	}
}