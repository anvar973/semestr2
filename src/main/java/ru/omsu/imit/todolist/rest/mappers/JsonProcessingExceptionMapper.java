package ru.omsu.imit.todolist.rest.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import ru.omsu.imit.todolist.exception.TodoListException;
import ru.omsu.imit.todolist.utils.ErrorCode;
import ru.omsu.imit.todolist.utils.TodoLIstUtils;


@Provider
public class JsonProcessingExceptionMapper implements
		ExceptionMapper<JsonProcessingException> {
	@Override
    public Response toResponse(JsonProcessingException exception) {
		return TodoLIstUtils.failureResponse(Status.BAD_REQUEST, new TodoListException(ErrorCode.JSON_PARSE_EXCEPTION));
    }
}