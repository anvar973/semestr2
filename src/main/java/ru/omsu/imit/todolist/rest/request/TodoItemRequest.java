package ru.omsu.imit.todolist.rest.request;

public class TodoItemRequest {

    private String text;
    
    public TodoItemRequest(String text) {
		super();
		this.text = text;
    }

	protected TodoItemRequest() {
    }

	public String getText() {
		return text;
	}


}