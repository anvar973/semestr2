package ru.omsu.imit.todolist.database;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import ru.omsu.imit.todolist.model.TodoItem;

public class DataBaseEmulator {
	private static List<TodoItem> list = new ArrayList<>();
	private static AtomicInteger atomicInteger = new AtomicInteger(1);

	public static void addItem(TodoItem item) {
		item.setId(atomicInteger.getAndIncrement());
		synchronized (list) {
			list.add(item);
		}
	}

	public static TodoItem getById(int id) {
		synchronized (list) {
			for (TodoItem item : list) {
				if (item.getId() == id)
					return item;
			}
			return null;
		}
	}

	public static List<TodoItem> getAll() {
		synchronized (list) {
			List<TodoItem> copyList = new ArrayList<>();
			for (TodoItem item : list) {
				TodoItem copyItem = new TodoItem(item.getId(), item.getText());
				copyList.add(copyItem);
			}
			return copyList;
		}
	}

	public static boolean deleteById(int id) {
		synchronized (list) {
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() == id) {
					list.remove(i);
					return true;
				}
			}
			return false;
		}
	}

	public static boolean editItemById(int id, String newText) {
		synchronized (list) {
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() == id) {
					list.get(i).setText(newText);
					return true;
				}
			}
			return false;
		}
	}

	public static void clear() {
		synchronized (list) {
			list.clear();
		}

	}

}
