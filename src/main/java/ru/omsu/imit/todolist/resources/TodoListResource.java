package ru.omsu.imit.todolist.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import ru.omsu.imit.todolist.service.TodoService;

@Path("/api")
public class TodoListResource {

    private static TodoService todoService = new TodoService();

    @POST
    @Path("/todolist")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addTodoItem(String json) {
    	return todoService.insertTodoItem(json);
    }
    
    @GET
    @Path("/todolist/{id}")
    @Produces("application/json")
    public Response getById(@PathParam(value = "id") int id) {
    	return todoService.getById(id);
    }
    
    @GET
    @Path("/todolist/")
    @Produces("application/json")
    public Response getAll() {
    	return todoService.getAll();
    }

    @PUT
    @Path("/todolist/{id}")
    @Produces("application/json")
    public Response editById(@PathParam(value = "id") int id, String json) {
    	return todoService.editById(id, json);
    }

    @DELETE
    @Path("/todolist/{id}")
    @Produces("application/json")
    public Response deleteById(@PathParam(value = "id") int id, String json) {
    	return todoService.deleteById(id, json);
    }
}

