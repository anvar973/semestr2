DROP DATABASE IF EXISTS mydb;
CREATE DATABASE mydb;
USE mydb;

CREATE TABLE developer (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL UNIQUE,
  PRIMARY KEY (id)
  );
  
  CREATE TABLE category (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) NOT NULL,
  PRIMARY KEY (id)
  );
  
  CREATE TABLE program (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  creation_date DATE NOT NULL,
  last_update_date DATE NOT NULL,
  developer_id INT(11) NOT NULL,
  category_id INT(11) NOT NULL,
  PRIMARY KEY (id),
	UNIQUE KEY program_FK (name, creation_date, developer_id, category_id),
  INDEX `fk_program_developer_idx` (`developer_id` ASC) VISIBLE,
  INDEX `fk_program_category_idx` (`category_id` ASC) VISIBLE,
  CONSTRAINT `fk_program_developer`
    FOREIGN KEY (`developer_id`)
    REFERENCES `mydb`.`developer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_program_category`
    FOREIGN KEY (`category_id`)
    REFERENCES `mydb`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


select * from category;
select * from developer;
select * from program;
